package com.harding.android.tictactoe;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.harding.tictactoe.TicTacToeGame;

public class AndroidTicTacToeActivity extends Activity {
	private TicTacToeGame game;
	private Button[] boardButtons;
	private TextView infoTextView;
	private boolean gameOver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boardButtons = new Button[TicTacToeGame.BOARD_SIZE];
        boardButtons[0] = (Button) findViewById(R.id.one);
        boardButtons[1] = (Button) findViewById(R.id.two);
        boardButtons[2] = (Button) findViewById(R.id.three);
        boardButtons[3] = (Button) findViewById(R.id.four);
        boardButtons[4] = (Button) findViewById(R.id.five);
        boardButtons[5] = (Button) findViewById(R.id.six);
        boardButtons[6] = (Button) findViewById(R.id.seven);
        boardButtons[7] = (Button) findViewById(R.id.eight);
        boardButtons[8] = (Button) findViewById(R.id.nine);
        infoTextView = (TextView) findViewById(R.id.information);
        game = new TicTacToeGame();
        startNewGame();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == R.id.menu_new_game) {
    		startNewGame();
    	}
    	else {
    		
    	}
    	return true;
    }
    
    private void startNewGame() {
    	gameOver = false;
    	game.clearBoard();
    	for(int i = 0; i < boardButtons.length; i++) {
    		boardButtons[i].setText("");
    		boardButtons[i].setEnabled(true);
    		boardButtons[i].setOnClickListener(new ButtonClickListener(i));
    	}
    	infoTextView.setText(R.string.first_human);
    }
    
    public class ButtonClickListener implements OnClickListener {
    	int location;
    	 
    	public ButtonClickListener(int location) {
    	this.location = location;
    	}
    	
    	public void setMove(char player, int location) {
    		game.setMove(player, location);
    		boardButtons[location].setEnabled(false);
    		boardButtons[location].setText(String.valueOf(player));
    		if(player == TicTacToeGame.HUMAN_PLAYER) {
    			boardButtons[location].setTextColor(Color.rgb(0, 200, 0));
    		}
    		else {
    			boardButtons[location].setTextColor(Color.rgb(200, 0, 0));
    		}
    	}


		public void onClick(View v) {
			if(boardButtons[location].isEnabled()) {
				setMove(TicTacToeGame.HUMAN_PLAYER, location);
				
				int winner = game.checkForWinner();
				if (winner == 0) {
					infoTextView.setText(R.string.turn_computer);
					int move = game.getComputerMove();
					setMove(TicTacToeGame.COMPUTER_PLAYER, move);
					winner = game.checkForWinner();
				}
				
				if(winner == 0) {
					infoTextView.setText(R.string.turn_human);
				}
				else if(winner == 1) {
					infoTextView.setText(R.string.result_tie);
					gameOver = true;
					for(Button b : boardButtons) {
						b.setEnabled(false);
					}
					return;
				}
				else if(winner == 2) {
					infoTextView.setText(R.string.result_human_wins);
					gameOver = true;
					for(Button b : boardButtons) {
						b.setEnabled(false);
					}
					return;
				}
				else {
					infoTextView.setText(R.string.result_computer_wins);
					gameOver = true;
					for(Button b : boardButtons) {
						b.setEnabled(false);
					}
					return;
				}
			}
		}

	}

}
